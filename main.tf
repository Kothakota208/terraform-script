provider "azurerm" {
  
  version         = "=2.0.0"
  features {}
}



# Create the resoure group for the resources
resource "azurerm_resource_group" "resource_group" {
  name     = "${var.prefix}-${var.resource_group_name}"
  location = var.resource_location

}


# Creates the virtual network for the resources
resource "azurerm_virtual_network" "vnet1" {

  name                = "${var.prefix}-${var.VirtualNetwork}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  address_space       = var.VirtualNetwork_AddressSpace
}


# Create the default subnet for the vnet
resource "azurerm_subnet" "subnet-default" {

  name                 = lookup(element(var.Subnet_Addresses, count.index), "name")
  count                = length(var.Subnet_Addresses)
  resource_group_name  = azurerm_resource_group.resource_group.name
  virtual_network_name = azurerm_virtual_network.vnet1.name
  address_prefix       = lookup(element(var.Subnet_Addresses, count.index), "ip")
}




resource "azurerm_app_service" "appservice" {
  name                = "${var.prefix}-${var.appservice_name}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  app_service_plan_id = azurerm_app_service_plan.appserviceplan1.id
  https_only          = true


  site_config {
    linux_fx_version = "NODE|10.14"
  }

  app_settings = {
    "SOME_KEY" = "some-value"
    #  "APPINSIGHTS_JAVASCRIPT_ENABLED" = truey

    "APPINSIGHTS_INSTRUMENTATIONKEY" = azurerm_application_insights.appService-app_insights.instrumentation_key

    "APPINSIGHTS_PROFILERFEATURE_VERSION" = "1.0.0"

    "DIAGNOSTICS_AZUREBLOBRETENTIONINDAYS" = "35"
    "WEBSITE_HTTPLOGGING_RETENTION_DAYS"   = "35"


    # Enable the following settings to map the app insights to the App service
    "ApplicationInsightsAgent_EXTENSION_VERSION" = "~2"
    # "XDT_MicrosoftApplicationInsights_Mode"= "recommended"
    # "APPINSIGHTS_PROFILERFEATURE_VERSION" = "1.0.0"
    # "DiagnosticServices_EXTENSION_VERSION" = "~3"
    # "APPINSIGHTS_SNAPSHOTFEATURE_VERSION" = "1.0.0"
    # "SnapshotDebugger_EXTENSION_VERSION" = "~1"
    # "InstrumentationEngine_EXTENSION_VERSION" = "~1"
    #  "XDT_MicrosoftApplicationInsights_BaseExtensions" = "~1"
  }

  connection_string {
    name  = "Database"
    type  = "SQLServer"
    value = "Server=some-server.mydomain.com;Integrated Security=SSPI"
  }


  depends_on = [
    azurerm_app_service_plan.appserviceplan1,
    azurerm_application_insights.appService-app_insights
  ]
}


# create the AppService Plan for the App Service hosting our website
resource "azurerm_app_service_plan" "appserviceplan1" {

  name                = "${var.prefix}-${var.app_service_plan_name}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  kind                = "linux"
  reserved            = true
  sku {
    tier = "Standard" # 
    size = "S1"
  }

}

# Create the App Service Autoscale settings
resource "azurerm_monitor_autoscale_setting" "appserviceplan1_autoscale" {
  name                = "{var.prefix}-{var.AppService1_AutoscaleSetting}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  target_resource_id  = azurerm_app_service_plan.appserviceplan1.id
  profile {
    name = "defaultProfile"

    capacity {
      default = 2
      minimum = 2
      maximum = 4
    }



    # Increase the Instance count by 1 when CPU usage is 85% consitanty for  5 mins
    rule {
      metric_trigger {
        metric_name        = "CpuPercentage"
        metric_resource_id = azurerm_app_service_plan.appserviceplan1.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 85
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

    # Decrease the Instance count by 1 when CPU usage is 40% or less consitanty for  5 mins
    rule {
      metric_trigger {
        metric_name        = "CpuPercentage"
        metric_resource_id = azurerm_app_service_plan.appserviceplan1.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 40
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }


    # Increase the Instance count by 1 when Memory usage is 85% consitanty for  5 mins
    rule {
      metric_trigger {
        metric_name        = "MemoryPercentage"
        metric_resource_id = azurerm_app_service_plan.appserviceplan1.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 85
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

    # Decrease the Instance count by 1 when Memory usage is 40% or less consitanty for  5 mins
    rule {
      metric_trigger {
        metric_name        = "MemoryPercentage"
        metric_resource_id = azurerm_app_service_plan.appserviceplan1.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 40
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

  }


}


resource "azurerm_postgresql_server" "postgressql_server" {

  name                = "${var.prefix}-${var.Postgresql_Server}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name

  sku_name = "B_Gen5_1"

  storage_profile {
    storage_mb            = 20480
    backup_retention_days = 7
    geo_redundant_backup  = "Disabled"

  }

  # Move this to azure vault
  administrator_login          = "psqladminun"
  administrator_login_password = "H@Sh1CoR3!"
  version                      = "11"

  ssl_enforcement = "Enabled"



}

resource "azurerm_postgresql_database" "postgressql_db" {
  name                = var.Postgresql_DB
  resource_group_name = azurerm_resource_group.resource_group.name
  server_name         = azurerm_postgresql_server.postgressql_server.name
  charset             = "UTF8"
  collation           = "English_United States.1252"

}


# Create random numbers  for DNS config and Traffic manager name
resource "random_id" "server" {
  keepers = {
    azi_id = 1
  }

  byte_length = 8
}


# Create the Traffice Manager profile
resource "azurerm_traffic_manager_profile" "traffic_manager" {
  name                   = "${var.prefix}-${var.TrafficManager_Profile}"
  resource_group_name    = azurerm_resource_group.resource_group.name
  traffic_routing_method = var.TrafficManager_Routing_Method

  # Setup the cname record in the DNS server so that DNS record is mapped to the Trafficmanger url
  dns_config {
    relative_name = random_id.server.hex
    ttl           = 100
  }
  monitor_config {
    protocol                     = "http"
    port                         = 80
    path                         = "/"
    interval_in_seconds          = 30
    timeout_in_seconds           = 9
    tolerated_number_of_failures = 3
  }

}



# Create the Traffic Manager Endpoint

resource "azurerm_traffic_manager_endpoint" "traffic_manager_endpoint" {
  name                = "${var.prefix}-${var.TrafficManager_Endpoint}"
  resource_group_name = azurerm_resource_group.resource_group.name
  profile_name        = azurerm_traffic_manager_profile.traffic_manager.name
  type                = var.TrafficManager_Endpoint_Type
  target_resource_id  = azurerm_app_service.appservice.id
  weight              = "1"

}



# Create the Application Insight for the app services

resource "azurerm_application_insights" "appService-app_insights" {

  name                = "${var.prefix}-${var.App_Insights}"
  location            = azurerm_resource_group.resource_group.location
  resource_group_name = azurerm_resource_group.resource_group.name
  application_type    = "web" # Node.JS ,java
}

output "instrumentation_key" {
  value = azurerm_application_insights.appService-app_insights.instrumentation_key
}

output "app_id" {
  value = azurerm_application_insights.appService-app_insights.app_id
}


